# Indee Labs Process All Files in a Directory
# S. Kevin McNeill, shonn.mcneill@mac.com
# 
# Introduction
# This file process a data file and plots all the data and all the peaks.

# Import libraries and write settings here.
import sys
import glob
import warnings
warnings.filterwarnings("ignore")
import click
import pandas as pd
import numpy as np
from scipy.signal import find_peaks, peak_widths
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

# Plot fonts and size.
font = {'family' : 'Times New Roman',
        'weight' : 'normal',
        'size'   : 14}
text = {'usetex' : 'true'}
plt.matplotlib.rc('font', **font)
plt.matplotlib.rc('text')
golden_ratio = 1.61803398875
h = 10
w = h*golden_ratio

# Functions

def read_txt(file_path, dateColumnName, encoding):
  '''
  Returns a pandas dataframe based on the data file and path (arg1:str)
  with a pandas date column (arg2:str) and with encoding (arg3:str).
  '''
  click.secho('Reading the data filess...', fg='yellow')
  dataframe = pd.read_csv(open(file_path,'rU'), parse_dates=dateColumnName, encoding = encoding, error_bad_lines=False)
  return dataframe

def normalize_time_to_first_row(dataframe, dateColumnName):
  '''
  Returns a pandas dataframe (arg1) with the first row of a column
  (arg2:str) subtracted from all other rows of the same column setting the 
  first row to zero (normalized).
  '''
  click.secho('Converting dates to seconds...', fg='yellow')
  dataframe[dateColumnName] -= dataframe[dateColumnName].iloc[0]
  return dataframe

def rename_column_name(dataframe, oldColumnName, newColumnName):
  '''
  Returns a pandas dataframe (arg1) renaming the old column
  name (arg2:str) to a new column name (arg3:str).
  '''
  dataframe = dataframe.rename(index=str, columns={oldColumnName: newColumnName});
  return dataframe

def convert_date_column_to_seconds(dataframe, columnName):
  '''
  Returns a pandas dataframe (arg1) with the date column
  (arg2:str) converted from a date to total seconds.
  '''
  dataframe[columnName] = dataframe[columnName].dt.total_seconds();
  return dataframe

def set_new_index(dataframe, columnName):
  '''
  Returns a pandas dataframe (arg1) with a new index column (arg2:str).
  '''
  dataframe = dataframe.set_index(columnName)
  return dataframe

def smooth_dataframe_col(dataframe, oldColumnName, newColumnName, num_points):
  '''
  Returns a dataframe (arg1) in which a column (arg2:str) has been smoothed
  using a rolling average of N points (arg4:int) and saved to a new column 
  called (arg3:str)
  '''
  click.secho('Smoothing the data...', fg='yellow')
  dataframe[newColumnName] = dataframe[oldColumnName].rolling(num_points, win_type='blackman').mean()
  return dataframe

def cleanup_data_file(data_file):
  '''
  Returns a dataframe (arg1) with the date/time column normalized (start at zero)
  and set as then index of the dataframe.
  '''
  dateCol = ['Time Stamp']
  encoding = 'ISO-8859-1'
  df = read_txt(data_file, dateCol, encoding)
  df = normalize_time_to_first_row(df, dateCol)
  oldColumnName = 'Time Stamp'
  newColumnName = 'Time [s]'
  df = rename_column_name(df, oldColumnName, newColumnName)
  df = convert_date_column_to_seconds(df, newColumnName)
  df = set_new_index(df, newColumnName)
  return df

def find_pks(df):
  '''
  Returns the dataframe (arg1) indexes of the peaks.
  '''
  click.secho('Finding the peaks...', fg='yellow')
  pressure_peaks, _ = find_peaks(df, 
                          width = 2, 
                          threshold = None, 
                          distance = 5,
                          prominence = 10);
  return pressure_peaks

def width_of_pks(dataframe, peaks, rh):
  '''
  Returns an array of peak widths given the dataframe (arg1), the peaks (arg2), and the relative height (arg3)
  '''
  click.secho('Finding the peak widths...', fg='yellow')
  pressure_peaks_width = peak_widths(dataframe, peaks, rel_height=rh);
  return pressure_peaks_width

def set_widths_pks(pressure_peaks_width, margin):
  '''
  Returns an array of peak widths that are rounded to the nearest integer and wider than
  the original width by the margin.
  '''
  width = pressure_peaks_width[0]
  height = pressure_peaks_width[1]
  left_side = pressure_peaks_width[2]
  right_side = pressure_peaks_width[3]
  left_side_rnd = np.round((left_side - width*margin),0)
  left_side_rnd = left_side_rnd.astype(int)
  # check if any of the times are negative and if true replace with 0
  left_side_rnd[left_side_rnd < 0] = 0
  right_side_rnd = np.round((right_side + width*margin),0)
  right_side_rnd = right_side_rnd.astype(int)
  return left_side_rnd, right_side_rnd

def plot_peaks(df, w, h, left_side, right_side, file):
  '''
  Returns a pdf file containing plots of the dataframe (arg1:dataframe) showing all
  the peaks and then each individual peak.  The width (arg2:float) and height (arg3:float)
  set the size of each plot.  The left (arg4:float) and right (arg5:float) span of each
  plot is set.  The file name (arg6:str) is used in the pdf output and plot titles.
  '''
  n = np.size(left_side) # sets the number of peak plots
  with PdfPages(file[:-4] + '.pdf') as pdf: #routes all figsave output to a pdf file
    message1 = 'Writing plot {0} to {1}.pdf'.format(1, file[:-4])
    click.secho(message1, fg='yellow') # screen log
    fig, ax = plt.subplots(figsize=(w, h)); # setup the plot for all peaks
    df.plot.line(subplots=True, grid=True, sharex=True, ax=ax);
    ax.annotate('Hell', [1,2]);
    [ax.legend(loc=4) for ax in plt.gcf().axes];
    suptitle = file[:-4] + ' All Peaks'
    fig.suptitle(suptitle, fontsize=16);
    fig.tight_layout(rect=[0, 0.03, 1, 0.95]);
    pdf.savefig(fig)
    click.secho('Plotting all the peaks...', fg='yellow')
    for i in range(n):
      fig1, ax = plt.subplots(3,1,figsize=(w, h));
      _ = df.iloc[left_side[i]:right_side[i], 0:3].plot.line(subplots=True, grid=True, sharex=True, ax=ax);
      fig1.suptitle(file[:-4] + ' Peak ' + str(i+1), fontsize=16);
      fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
      pdf.savefig(fig);
      message2 = 'Writing plot {0} to {1}.pdf'.format(i+2, file[:-4])
      # click.secho(message2, fg='yellow')
  return

# @click.command()
# @click.option('--data_file', help='--data_file = yourdatafile.txt')
# @click.option('--input_flag', default='single', help='--input_flag = single OR all OR email')
def run_plots(data_file, input_flag):
  '''
  Simple program that ...
  '''
  if input_flag == 'single':
    df = cleanup_data_file(data_file)
    save_plot = True
    #plot_all_time_all_columns(df,w,h,data_file,save_plot)
    oldColName = 'Absolute Pressure [PSIA]'
    newColName = 'smooth_pressure'
    number_of_pts_average = 100
    smooth_dataframe_col(df,oldColName,newColName, number_of_pts_average)
    pressure_peaks = find_pks(df[newColName])
    relative_height = 0.9
    pressure_peak_width = width_of_pks(df[newColName], pressure_peaks, relative_height)
    margin_to_add_to_width = 0.6
    lft_side, rht_side = set_widths_pks(pressure_peak_width, margin_to_add_to_width)
    plot_peaks(df,w,h,lft_side,rht_side,data_file)
    return
  
  if input_flag == 'all':
    for file_name in glob.glob('Alicat Data Log*.txt'):
      click.echo(file_name)
      df = cleanup_data_file(file_name)
      save_plot = True
      #plot_all_time_all_columns(df, w, h, file_name, save_plot)
      oldColName = 'Absolute Pressure [PSIA]'
      newColName = 'smooth_pressure'
      number_of_pts_average = 100
      smooth_dataframe_col(df,oldColName,newColName, number_of_pts_average)
      pressure_peaks = find_pks(df[newColName])
      relative_height = 0.9
      pressure_peak_width = width_of_pks(df[newColName], pressure_peaks, relative_height)
      margin_to_add_to_width = 0.6
      lft_side, rht_side = set_widths_pks(pressure_peak_width, margin_to_add_to_width)
      plot_peaks(df,w,h,lft_side,rht_side,file_name)        
    return

  if input_flag == 'email':
    df = cleanup_data_file(data_file)
    return click.secho('This feature is not yet developed', fg='red')

click.secho(sys.argv[2], fg='yellow')
# if __name__ == '__main__':
run_plots(sys.argv[2], 'single')