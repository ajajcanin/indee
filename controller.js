import { Router } from "express";
import fs from "fs-extra";
import path from "path";
import multer from "multer";
import nodemailer from "nodemailer";
import { PythonShell } from "python-shell";
import { zip } from "zip-a-folder";
import {s3} from "./";
import * as cron from 'node-cron';
import * as axios from 'axios';

const SCRIPT_PATH = path.join(__dirname, "python/script.py");

const AUTH = {
    USER: 'c',
    PASS: 'BJUc4G7oaOcx0+kcPXfvT9hgVWR200NXZsQJD1CpmQ1b'
};

let FILE_NAMES = [];

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, `${__dirname}/uploads/`);
  },
  filename: async function(req, file, cb) {
    await fs.ensureDir(path.join(__dirname, `uploads/${req.context}`));
    const filename = `${req.context}/csv${file.originalname.slice(
      file.originalname.lastIndexOf(".")
    )}`;
    req.uploadedFile = filename;
    cb(null, filename); //Appending .jpg
  }
});

const upload = multer({ storage });

const router = new Router();

const getObject = async (key) => new Promise(
    (resolve) => {
        //construct getParam
        var getParams = {
            Bucket: 'flowmeter',
            Key: key
        }
        //Fetch or read data from aws s3
        s3.getObject(getParams, function (err, data) {
            if (err) {
                console.log(err);
            }
            resolve(data.Body);
        })
    }
);

const deleteFileFromBucket = async (fileName) => {
    var done = function (err, data) {
        if (err) console.log(err);
        else console.log(data);
    };
    s3.listObjects({ Bucket: 'flowmeter', Prefix: '' }, async function (err, data) {
        if (data.Contents.length) {
            await Promise.all(data.Contents.map(async file => {
                const filename = file.Key.split('/');
                if (filename[filename.length - 1] == fileName) {
                    s3.delete({
                        Bucket: 'flowmeter',
                        Key: fileName
                    })
                }
            }))
        }
    }, done);
}

cron.schedule('* */5 * * * * *', () => {
    axios.post('http://localhost:5000/generate');
});


router.post(
    `/`,
async (req, res, next) => {
    req.context = String(Date.now());
    req.fileName = req.context;
    const contentPath = path.join(__dirname, `uploads/${req.context}`);
    await fs.ensureDir(contentPath);
    await fs.mkdir(contentPath, () => {
        const promise = new Promise((resolve, reject) => s3.listObjects({
            Bucket: 'flowmeter',
            Delimiter: '/',
            Prefix: ''
        }, (err, data) => {
            if (err)
                reject(err);
            data.Contents.forEach(async content => {
                const contentObject = await getObject(content.Key);
                FILE_NAMES.push(content.Key);
                fs.writeFile(contentPath + `/${content.Key}`, contentObject, error => {
                    if (error) console.log(error);
                })
            });
            resolve(data.Contents)
        }));
        next();
    });
},
upload.single('csv'),
async (req, res) => {
    let transporter = nodemailer.createTransport({
        host: "email-smtp.us-east-1.amazonaws.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: AUTH.USER, // generated ethereal user
            pass: AUTH.PASS // generated ethereal password
        }
    });

    const location = path.join(__dirname, `uploads/${req.context}/`);
    const zipLocation = path.join(__dirname, `uploads/${req.context}.zip`);
    const dataLocation = path.join(__dirname, `uploads/${req.context}/csv.txt`);
    const pyScriptLocation = path.join(
        __dirname,
        `uploads/${req.context}/script.py`
    );
    console.log("GENERATING PLOTS", req.body);
    let email = null;
    try {
        const p = JSON.parse(req.body.data);
        email = p.email;
    } catch (__) {}
    await fs.copyFile(SCRIPT_PATH, pyScriptLocation);
    res.send({ msg: req.uploadedFile });

    // fs.ensu
    console.log("SENDING ARGUNMENTS", [location, dataLocation])
    PythonShell.run(
        pyScriptLocation,
        { args: [location, dataLocation] },
        async err => {
            if (err) {
                console.log("ERR MES", err.message, err)
                return;
            };
            console.log("Removing scripts")
            await fs.remove(pyScriptLocation);
            await fs.remove(dataLocation);
            console.log("Zipping")

            await zip(location, zipLocation);
            console.log("Sending")

            await transporter.sendMail({
                from: '"Indee Labs" <flowmeter@indeelabs.com>', // sender address
                to: req.body.email || email, // list of receivers
                subject: "Indee generated graphs", // Subject line
                text:
                    "Thank you for your submission.\r\nYour generated report is in the attachment.", // plain text body
                html:
                    "<P>Thankk you for your submission.</p><p>Your generated report is in the attachment.</p>", // html body
                attachments: [
                    {
                        path: zipLocation
                    }
                ]
            });
            console.log("Removing")

            await fs.remove(zipLocation);
            await fs.remove(location);
        }
    );
    FILE_NAMES.forEach(async fileName => {
        await deleteFileFromBucket(fileName);
    });
    FILE_NAMES = [];
}
);


export default router;
