import express from "express";
import bodyParser from "body-parser";
import http from "http";
import generateRoutes from "./controller";
import * as AWS from "aws-sdk";

const app = express();

const server = http.createServer(app);

const port = process.env.PORT || 5000;

export const s3 = new AWS.S3({
  region: 'us-west-1',
  albumBucketName: 'flowmeter',
  accessKeyId: 'AKIAVL6LIGVMLS5PHXF7',
  secretAccessKey: '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',
});

app.use((req, res, next) => {
  res.setHeader(
    "Access-Control-Allow-Methods",
    "PUT, POST, GET, DELETE, OPTIONS"
  );
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Authorization, Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use(bodyParser.json());

app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "100mb"
  })
);

app.use(`/generate`, generateRoutes);

app.use(express.static("public"));

app.use("/test", (req, res) =>
  res.status(200).send(`
  <!DOCTYPE html>
  <html>
  <head>
    <title></title>
  </head>
  <body>
    Hello
  </body>
  </html>
`)
);

server.listen(port, () => {
  console.log(`Server app started on port ${port}`);
});
